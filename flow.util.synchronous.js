//从条目拆分同步数据到工作量评估

/**
 * 数据源
 */
var datasource = "cpmis_ds";

/**
 * 初始化工作量评估Tab
 * @param {*} docId 
 */
function synchronous(docId) {
    var docProcess = getDocumentProcess();
    var formProcess = getFormProcess();
    var domainid = getDomainid();
    var newForm1 = formProcess.doViewByFormName(Form_Name.WORK_LOAD, getApplication());
    var newForm2 = formProcess.doViewByFormName(Form_Name.PROJECT_PLAN, getApplication());
    var newForm3 = formProcess.doViewByFormName(Form_Name.CLAUSE_THIN, getApplication());
    if (isNotNull(docId)) {
        createWorkloadInfo(domainid, docId, docProcess, newForm1);
        createProjectScheme(domainid, docId, docProcess, newForm2);
        createClauseThin(domainid, docId, docProcess, newForm3);
        deleteClauseThin(domainid, docId);
        deleteWorkload(domainid, docId);
        deleteProjectScheme(domainid, docId);
        recomputation(domainid, docId);
    }
}

/**
 * 重新计算每个工作量评估的投入人力，防止工作量为更新，但是细化条目增加（缺少）了
 */
function recomputation(domainid, docId) {

    var sql = "zhgs_xqfxgzl";
    var my_work_sql = "select '" + domainid + "' AS domainid, group_concat(id) as item_ids from tlk_workload_assess_info where parent = '" + docId + "' ";
    var result_1 = findBySQL(my_work_sql);
    var strArray = result_1.getItemValueAsString("ids");
    var workList = splitText(strArray, ",");
    for (var t = 0; t < workList.length; t++) {
        var workId = workList[t];
        //1. 更新开发工作量
        var sql_1 = "update tlk_workload_assess_info " +
            " set " +
            " item_zhgs_xqfxgzl = (select sum(item_xhtm_xqfx) from tlk_clause_thin where parent = '" + workId + "') " +
            " , item_zhgs_sjkfgzl = (select sum(item_xhtm_sjkf) from tlk_clause_thin where parent = '" + workId + "') " +
            " , item_zhgs_uiitcs = (select sum(item_xhtm_test) from tlk_clause_thin where parent = '" + workId + "') " +
            " where parent = '" + docId + "' and (ITEM_ZTKFS = '中汇公司' or ITEM_ZTKFS is null) ";
        var sql_2 = "update tlk_workload_assess_info " +
            " set " +
            " item_zhgs_xqfxgzl = (select sum(item_wbgs_xqfxgzl) from tlk_clause_thin where parent = '" + workId + "') " +
            " , item_zhgs_sjkfgzl = (select sum(item_wbgs_sjkfgzl) from tlk_clause_thin where parent = '" + workId + "') " +
            " , item_zhgs_uiitcs = (select sum(item_wbgs_uiitcs) from tlk_clause_thin where parent = '" + workId + "')" +
            " where parent = '" + docId + "' and ITEM_ZTKFS = '外包公司' ";
        updateByDSName(datasource, sql_1);
        updateByDSName(datasource, sql_2);

        //2. 更新中汇、外包工作量
        var update_zh_and_wb_sql = "update tlk_workload_assess_info set " +
            " item_zhgzl_zj = ( " +
            " item_zh_kfxtcs + item_zh_xmgl +  item_zh_qt + item_zh_xtgncs + item_zh_xtxncs + item_zh_xtpzbs  " +
            " + item_zh_yhzc + item_zh_zzjgl + item_zh_ysgncs + item_zh_xnyscs + item_zh_xtpzcsbs + item_zh_yscsjf  " +
            " + item_zh_gnmncs + item_zh_xnmncs + ( " +
            " case when (item_ztkfs = '中汇公司' or item_ztkfs = null) then  " +
            " ( " +
            " select (sum(item_xhtm_xqfx) +  sum(item_xhtm_sjkf) +  sum(item_xhtm_test)) as count from tlk_clause_thin where parent = '" + workId + "' " +
            " )else 0 end) " +
            " )  " +
            " , item_zhgzl_zs = ( " +
            " item_wb_kfxtcs + item_wb_xmgl +  item_wb_qt + item_wb_xtgncs + item_wb_xtxncs + item_wb_xtpzbs  " +
            " + item_wb_yhzc + item_wb_zzjgl + item_wb_ysgncs + item_wb_xnyscs + item_wb_xtpzcsbs + item_wb_yscsjf  " +
            " + item_wb_gnmncs + item_wb_xnmncs + ( " +
            " case when (item_ztkfs = '外包公司') then  " +
            " ( " +
            " select (sum(item_xhtm_xqfx) +  sum(item_xhtm_sjkf) +  sum(item_xhtm_test)) as count from tlk_clause_thin where parent = '" + workId + "' " +
            " )else 0 end) " +
            " ) " +
            " where id = '" + workId + "' ";
        updateByDSName(datasource, update_zh_and_wb_sql);

        //3. 更新总计
        var update_zj_sql = "update tlk_workload_assess_info set " +
            " item_gzlzj = item_zhgzl_zj + item_wbgzl_zj " +
            " , item_zhgzl_zs = round(item_zhgzl_zj/20.83, 4) " +
            " , item_wbgzl_zs = round(item_wbgzl_zj/20.83, 4) " +
            " , item_gzl_zs = round((item_zhgzl_zj + item_wbgzl_zj)/20.83, 4) where id = '" + workId + "' ";
        updateByDSName(datasource, update_zj_sql);
    }
}

/**
 * 删除没有需求条目的项目安排计划
 */
function deleteProjectScheme(domainid, docId) {
    var sql = "select '" + domainid + "' AS domainid,  id as item_id from tlk_project_scheme ps where parent = '" + docId + "' " +
        " and  " +
        " (case when item_shixianzixitong is null or item_shixianzixitong = '' " +
        "  then concat(item_shixianxitong, item_shangxianbanben) " +
        "  else concat(item_shixianxitong, '-', item_shixianzixitong, item_shangxianbanben) end " +
        " ) not in ( " +
        " select ( " +
        "  case when (cp.item_xqzxt is null or cp.item_xqzxt = '') then concat(cp.item_xqsxxt, cp.item_dqbb) " +
        "  else concat(cp.item_xqsxxt, '-', cp.item_xqzxt, cp.item_dqbb) end  " +
        " ) from tlk_clause_present cp where cp.parent = '" + docId + "' " +
        " )";
    var dataList = queryBySQL(sql);
    if (dataList != null && dataList.size() > 0) {
        for (var i = 0; i < dataList.size(); i++) {
            var data = dataList.get(i);
            var id = data.getItemValueAsString("id");
            var sql_1 = "DELETE FROM tlk_project_scheme WHERE id='" + id + "'";
            var sql_2 = "DELETE FROM t_document WHERE ID='" + id + "'";
            deleteByDSName(datasource, sql_1);
            deleteByDSName(datasource, sql_2);
        }
    }
}

/**
 * 删除没有子任务的工作量评估
 */
function deleteWorkload(domainid, docId) {
    var sql = "select '" + domainid + "' AS domainid, id as item_id from tlk_workload_assess_info where parent = '" + docId + "'";
    var dataList = queryBySQL(sql);
    if (null != dataList && dataList.size() > 0) {
        for (var i = 0; i < dataList.size(); i++) {
            var data = dataList.get(i);
            var id = data.getItemValueAsString("id");
            var sql_1 = "select * from tlk_clause_thin where parent = '" + id + "'";
            var result_1 = findBySQL(sql_1);
            if (null == result_1) {
                var sql_2 = "DELETE FROM tlk_workload_assess_info WHERE ID='" + id + "'";
                var sql_3 = "DELETE FROM t_document WHERE ID='" + id + "'";
                deleteByDSName(datasource, sql_2);
                deleteByDSName(datasource, sql_3);
            }
        }
    }
}

/**
 * 删除不存在应用需求设计的细化条目
 */
function deleteClauseThin(domainid, docId) {
    var sql = "select '" + domainid + "' AS domainid, ct.id as item_id from tlk_clause_thin ct, tlk_workload_assess_info wa " +
        " where wa.parent = '" + docId + "' and wa.id = ct.parent " +
        " and not exists ( " +
        " select 1 from tlk_clause_present cp where cp.parent = '" + docId + "' and ct.item_xhtm_tmid = cp.id " +
        " ) ";
    var dataList = queryBySQL(sql);
    if (null != dataList && dataList.size() > 0) {
        for (var n = 0; n < dataList.size(); n++) {
            var data = dataList.get(n);
            var id = data.getItemValueAsString("id");
            var sql_1 = "delete from tlk_clause_thin where id = '" + id + "'";
            var sql_2 = "DELETE FROM t_document WHERE ID='" + id + "'";
            deleteByDSName(datasource, sql_1);
            deleteByDSName(datasource, sql_2);
        }
    }
}

/**
 * 创建细化条目
 * @param {*} domainid 
 * @param {*} docId 
 * @param {*} docProcess 
 * @param {*} newForm 
 */
function createClauseThin(domainid, docId, docProcess, newForm) {
    var sql_1 = "select '" + domainid + "' AS domainid, id as item_id, item_xtbb as item_xtbb from tlk_workload_assess_info" +
        " where parent = '" + docId + "'";
    var dataList = queryBySQL(sql_1);
    if (null != dataList && dataList.size() > 0) {
        for (var i = 0; i < dataList.size(); i++) {
            var data = dataList.get(i);
            var wId = data.getItemValueAsString("id");
            var wXt = data.getItemValueAsString("xtbb");
            var sql_2 = "select '" + domainid + "' AS domainid, ( " +
                " case when (cp.item_xqzxt is null or cp.item_xqzxt = '') then concat(cp.item_xqsxxt, cp.item_dqbb)  " +
                "  else concat(cp.item_xqsxxt, '-', cp.item_xqzxt, cp.item_dqbb) end ) as item_sys_ver " +
                "  , cp.item_xqsxxt as item_xqsxxt,  cp.item_dqbb as item_dqbb,cp.item_zt as item_zt, cp.id as item_id " +
                "  ,cp.item_tmbh as item_tmbh, CONCAT( cp.item_tmbh, '_', cp.item_zt) as item_title , cp.item_xmztc as item_xmztc " +
                " from tlk_clause_present cp where cp.parent = '" + docId + "' " +
                " and ( 	 " +
                " case when (cp.item_xqzxt is null or cp.item_xqzxt = '') then concat(cp.item_xqsxxt, cp.item_dqbb)  " +
                "  else concat(cp.item_xqsxxt, '-', cp.item_xqzxt, cp.item_dqbb) end  " +
                " ) = '" + wXt + "'  " +
                " and not exists ( " +
                "  select * from tlk_clause_thin ct  " +
                "  join tlk_workload_assess_info wa on ct.parent = wa.id " +
                "  where ct.item_tm_bh = cp.item_tmbh and ct.item_xhtm_xtbb = ( " +
                " case when (cp.item_xqzxt is null or cp.item_xqzxt = '') then concat(cp.item_xqsxxt, cp.item_dqbb)  " +
                "  else concat(cp.item_xqsxxt, '-', cp.item_xqzxt, cp.item_dqbb) end  " +
                "  ) and ct.ITEM_XHTM_XQTM = CONCAT( cp.item_tmbh, '_', cp.item_zt) and  wa.parent = '" + docId + "' " +
                " )";
            var dataList2 = queryBySQL(sql_2);
            if (null != dataList2 && dataList2.size() > 0) {
                for (var t = 0; t < dataList2.size(); t++) {
                    var data2 = dataList2.get(t);
                    var title = data2.getItemValueAsString("title"); //条目编号-条目主题 reqs = 
                    var title_1 = title; //为防止reqs指丢失，重新赋值                 
                    var tmbh = data2.getItemValueAsString("tmbh"); //条目编号
                    var zt = data2.getItemValueAsString("zt"); //条目主题  req_title
                    var xt = data2.getItemValueAsString("xqsxxt"); //系统
                    var zxt = data2.getItemValueAsString("xmztc"); //子系统
                    var dqbb = data2.getItemValueAsString("dqbb"); //当前版本
                    var version = data2.getItemValueAsString("sys_ver"); //
                    var tm_id = data2.getItemValueAsString("id");

                    var sql_3 = "SELECT  '" + domainid + "' AS domainid, id as item_id FROM tlk_clause_thin WHERE item_tm_bh='" + tmbh + "'";
                    var data3 = queryBySQL(sql_3);
                    if (null != data3 && data3.size() > 0) {
                        var sql_4 = "UPDATE tlk_clause_thin SET " +
                            " PARENT='" + wId + "' " +
                            ", item_xhtm_xtbb = '" + version + "'" +
                            ", item_xhtm_tmid = '" + tm_id + "'" +
                            ", item_tm_bh = '" + tmbh + "'" +
                            ", item_tm_zt = '" + zt + "'" +
                            ", item_tm_xt = '" + xt + "'" +
                            ", item_tm_zxt = '" + zxt + "'" +
                            ", item_tm_bb = '" + dqbb + "'" +
                            ", ITEM_XHTM_XQTM = '" + tmbh + "_" + zt + "'" +
                            "WHERE item_tm_bh='" + tmbh + "'";
                        updateByDSName(datasource, sql_4);
                        for (var iter = data3.iterator(); iter.hasNext();) {
                            var d = iter.next();
                            var did = d.getItemValueAsString("id");
                            var sql_5 = "UPDATE t_document SET PARENT='" + wId + "' WHERE ID='" + did + "'";
                            updateByDSName(datasource, sql_5);
                        }
                    } else {
                        var sqlll = "select '" + domainid + "' as domainid, id as item_id from tlk_workload_assess_info where item_xtbb = '" + version + "' and parent = '" + docId + "'";
                        var ress = findBySQL(sqlll);
                        var pId = ress.getItemValueAsString("id");
                        var newDoc = docProcess.doNew(newForm, getWebUser(), createParamsTable());
                        newDoc.setParent(pId);
                        newDoc.setAuthor(getWebUser().getId());
                        newDoc.setIstmp(false);
                        newDoc.setApplicationid(getApplication());
                        newDoc.setDomainid(getWebUser().getDomainid());
                        newDoc.addStringItem("xhtm_xqtm", title_1); //对应的条目标题
                        newDoc.addStringItem("xhtm_xtbb", version); //对应的系统版本
                        newDoc.addStringItem("xhtm_tmid", tm_id); //对应的条目ID
                        newDoc.addStringItem("tm_bh", tmbh); //对应的条目编号
                        newDoc.addStringItem("tm_zt", zt); //对应的条目主题
                        newDoc.addStringItem("tm_xt", xt); //对应的系统
                        newDoc.addStringItem("tm_zxt", zxt); //对应的子系统
                        newDoc.addStringItem("tm_bb", dqbb); //对应的当前版本
                        docProcess.doCreate(newDoc);
                    }
                }
            } else {

            }
        }
    }
}

/**
 * 创建项目整体安排计划
 * @param {*} domainid 
 * @param {*} docId 
 * @param {*} docProcess 
 * @param {*} newForm 
 */
function createProjectScheme(domainid, docId, docProcess, newForm) {
    var sql_1 = "select  DISTINCT '" + domainid + "' AS domainid, ( " +
        " case when (cp.item_xqzxt is null or cp.item_xqzxt = '') then concat(cp.item_xqsxxt, cp.item_dqbb)  " +
        " else concat(cp.item_xqsxxt, '-', cp.item_xqzxt, cp.item_dqbb) end  " +
        " ) as item_sys_ver, cp.item_xqsxxt as item_xqsxxt,  cp.item_xqzxt as item_xqzxt,  cp.item_dqbb as item_dqbb " +
        " from tlk_clause_present cp where cp.parent = '" + docId + "' ";
    var dataList = queryBySQL(sql_1);
    if (dataList != null || dataList.size() > 0) {
        for (var iter = dataList.iterator(); iter.hasNext();) {
            var data = iter.next();
            var version = data.getItemValueAsString("sys_ver");
            var xqsxxt = data.getItemValueAsString("xqsxxt"); //实现系统
            var xqzxt = data.getItemValueAsString("xqzxt"); //实现子系统
            var dqbb = data.getItemValueAsString("dqbb"); //系统版本
            var sqll = "select '" + domainid + "' AS domainid, group_concat(item_zt separator '==') as item_zt from tlk_clause_present t1 where " +
                " (" +
                " case when (t1.item_xqzxt is null or t1.item_xqzxt = '') then concat(t1.item_xqsxxt, t1.item_dqbb) " +
                " else concat(t1.item_xqsxxt, '-', t1.item_xqzxt, t1.item_dqbb) end " +
                " ) = '" + version + "' and t1.parent = '" + docId + "'";
            var res = findBySQL(sqll);
            var ztStr = res.getItemValueAsString("zt");
            var array = splitText(ztStr, "==");
            var content = "";
            for (var t = 0; t < (array.length >= 5 ? 5 : array.length); t++) {
                content = content + (t + 1) + ". " + array[t] + "    <br/>"
            }
            var sql_2 = "SELECT '" + domainid + "' AS domainid, id as item_id FROM tlk_project_scheme WHERE PARENT='" + docId + "'  AND " +
                " (case when (item_shixianzixitong is null or item_shixianzixitong = '')  " +
                " then concat(item_shixianxitong, item_shangxianbanben) " +
                " else concat(item_shixianxitong, '-', item_shixianzixitong, item_shangxianbanben) end) " +
                " ='" + version + "'";
            var result = findBySQL(sql_2);
            if (null != result) { //原来已经有对应系统版本的项目安排计划
                var oldId = result.getItemValueAsString("id");
                var sql_3 = "update tlk_project_scheme set item_xuqiushixianneirong = '" + content + "' where id = '" + oldId + "'";
                updateByDSName(datasource, sql_3);
            } else {
                var newDoc = docProcess.doNew(newForm, getWebUser(), createParamsTable());
                newDoc.setParent(docId);
                newDoc.setAuthor(getWebUser().getId());
                newDoc.setIstmp(false);
                newDoc.setApplicationid(getApplication());
                newDoc.setDomainid(getWebUser().getDomainid());
                newDoc.addStringItem("shixianxitong", xqsxxt);
                newDoc.addStringItem("shixianzixitong", xqzxt);
                newDoc.addStringItem("shangxianbanben", dqbb);
                newDoc.addStringItem("xuqiushixianneirong", content);
                docProcess.doCreate(newDoc);
            }
        }
    }
}

/**
 * 创建工作量评估
 * 查询出需求设计对应项目所有系统并且不存在于项目签报中
 */
function createWorkloadInfo(domainid, docId, docProcess, newForm) {
    var sql = "select  DISTINCT '" + domainid + "' AS domainid, ( " +
        " case when (cp.item_xqzxt is null or cp.item_xqzxt = '') then concat(cp.item_xqsxxt, cp.item_dqbb)  " +
        " else concat(cp.item_xqsxxt, '-', cp.item_xqzxt, cp.item_dqbb) end  " +
        " ) as item_sys_ver, cp.item_xqsxxt as item_xqsxxt,  cp.item_xqzxt as item_xqzxt,  cp.item_dqbb as item_dqbb " +
        " from tlk_clause_present cp where cp.parent = '" + docId + "' " +
        " and not exists ( " +
        "     select * from tlk_workload_assess_info wai where wai.parent = '" + docId + "' and wai.item_xtbb = ( " +
        " case when (cp.item_xqzxt is null or cp.item_xqzxt = '') then concat(cp.item_xqsxxt, cp.item_dqbb)  " +
        " else concat(cp.item_xqsxxt, '-', cp.item_xqzxt, cp.item_dqbb) end " +
        "     ) " +
        " )";
    var dataList = queryBySQL(sql);
    if (dataList != null || dataList.size() > 0) {
        for (var iter = dataList.iterator(); iter.hasNext();) {
            var data = iter.next();
            var version = data.getItemValueAsString("sys_ver");
            var xqsxxt = data.getItemValueAsString("xqsxxt"); //实现系统
            var xqzxt = data.getItemValueAsString("xqzxt"); //实现子系统
            var dqbb = data.getItemValueAsString("dqbb"); //系统版本

            var newDoc = docProcess.doNew(newForm, getWebUser(), createParamsTable());
            newDoc.setParent(docId);
            newDoc.setAuthor(getWebUser().getId());
            newDoc.setIstmp(false);
            newDoc.setApplicationid(getApplication());
            newDoc.setDomainid(getWebUser().getDomainid());
            newDoc.addStringItem("xtbb", version);
            newDoc.addStringItem("fkzt", "开发未反馈");
            docProcess.doCreate(newDoc);
        }
    }
}