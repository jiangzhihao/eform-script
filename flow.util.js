/**
 * 数据源
 */
var datasource = "cpmis_ds";

var lrk_boxs = ["xmfzr_lrk", "bmld_lrk", "zxld_lrk", "jsqk_lrk", "jskfb_lrk"];
var zsk_boxs = ["xmfzr_zsk", "bmld_zsk", "zxld_zsk", "jsqk_zsk", "jskfb_zsk"];
var FLOW_NAME = {
    E_FORM: "电子需求单",
    GO_BACK: "电子需求单调整"
}
var clause_state = {
    STATE_1: "正常",
    STATE_2: "正在回退",
    STATE_3: "已经回退"
}

/**
 * 流程标签
 */
var flow_state = {
    FLOW_0: "开始",
    FLOW_1: "需求提出",
    FLOW_2: "需求部门领导评审",
    FLOW_3: "设计部门领导评审",
    FLOW_4: "设计部门项目负责人评审",
    FLOW_5: "条目拆分",
    FLOW_6: "工作量评估",
    FLOW_7: "项目负责人评审",
    FLOW_8: "发起人指定领导",
    FLOW_9: "部门领导评审",
    FLOW_10: "提交中心领导",
    FLOW_11: "中心领导评审",
    FLOW_12: "实施",
    FLOW_13: "回填签报信息",
    FLOW_14: "结束",
    FLOW_15: "调整需求信息",
    FLOW_16: "填写签报信息"
}

/**
 * 工作量变化标记
 */
var WorkLoad_Type = {
    type1: "FLOW_6_TO_FLOW_7",
    type2: "FLOW_11_TO_FLOW_12",
    type3: "FLOW_11_TO_FLOW_6",
    type4: "FLOW_7_TO_FLOW_6",
    type5: "FLOW_12_TO_DELETE",
    type6: "BACK_BACK"
}



var Form_Name = {
    WORK_LOAD: "workload_assess_info", //
    PROJECT_PLAN: "project_scheme",
    CLAUSE_THIN: "clause_thin" //细化条目
}




/**
 * 获取项目关联的所有负责人
 */
function getMansFromForm() {
    //code
    var ids = "";

    var ids0 = getItemValueAsString("sqr"); //立项提出人
    var ids1 = getItemValueAsString("xqtcbm_pgry"); //需求提出部门
    var ids2 = getItemValueAsString("kfgs_pgry"); //开发公司
    var ids3 = getItemValueAsString("sjbm_pgry"); //设计部门
    var ids4 = getItemValueAsString("ywbm_pgry"); //运维部门

    ids += ids0;
    if (isNotNull(ids1) && ids1 != "") {
        //code
        ids += ";" + ids1;
    }
    if (isNotNull(ids2) && ids2 != "") {
        //code
        ids += ";" + ids2;
    }
    if (isNotNull(ids3) && ids3 != "") {
        //code
        ids += ";" + ids3;
    }
    if (isNotNull(ids4) && ids4 != "") {
        //code
        ids += ";" + ids4;
    }
    return ids;
}



/**
 *
 * 获取此次流程的主体负责人
 * 类型是 前置项目：发起人
 * 类型是 非前置项目：主体系统的开发部项目负责人
 */
function getFlowLeader(doc) {
    var id = "";
    type = getItemValueAsString("qzxm");
    if (isNotNull(type)) {
        if ("是".equals(type)) {
            if (isNotNull(doc)) {
                id = doc.getItemValueAsString("sqr");
            } else {
                id = getItemValueAsString("sqr");
            }

        } else if ("否".equals(type)) {
            var domainid = getDomainid();
            var main_sys = "";
            if (isNotNull(doc)) {
                main_sys = doc.getItemValueAsString("ztxt_mc");
            } else {
                main_sys = getItemValueAsString("ztxt_mc");
            }
            sql =
                "select " +
                "group_concat(item_tech) as item_data " +
                ", '" + domainid + "' as domainid " +
                "from tlk_system_manager  " +
                "where( " +
                "case when item_system_son is null or item_system_son = '' " +
                "then " +
                "item_system " +
                "else " +
                "concat(item_system, '_', item_system_son) " +
                "end " +
                ") in ( '" + main_sys + "' )";

            id = findBySQL(sql).getItemValueAsString("data");
        }
    }
    return id;
}


/**
 * 同步评审信息
 */
function tbpsxx(doc, n) {
    var now = getToday();
    var lrkBox = doc.findItem(lrk_boxs[n]);
    var zskBox = doc.findItem(zsk_boxs[n]);
    var msg = lrkBox.getValue();
    if (!isNotNull(msg)) {
        msg = "";
    }
    var user = getWebUser();
    msg = "     " + user.getName() + "：" + msg + "  " + getYear(now) + "-" + getMonth(now) + "-" + getDay(now) + "";
    var history = zskBox.getValue();
    if (isNotNull(history)) {
        msg = "<br/>" + msg;
    }
    msg = history + msg;
    zskBox.setValue(msg);
    lrkBox.setValue("");
}