var datasource = "cpmis_ds";

var fzh_flow_state = {
    FLOW_0: "开始",
    FLOW_1: "需求提出",
    FLOW_2: "需求部门领导评审",
    FLOW_3: "设计部门领导评审",
    FLOW_4: "设计部门项目负责人评审",
    FLOW_5: "条目拆分",
    FLOW_6: "结束",
    FLOW_7: "调整需求信息"
}

/**
 * 条目拆分保存之后、提交之后，清空 条件查询、批量修改 字段
 */
function clearSearchBoxesAndUpdateBoxes(doc) { //
    var NEED_CLEAR_BOX = ["search_tmbh", "search_tmzt", "search_sxxt", "search_sxzxt", "search_jhbb", "search_xqbm_fzr", "ms", "search_sjbm_fzr", "search_kfgs_fzr", "tcbm", "xqbm_fzr", "sjbm_fzr", "kfgs_fzr", "jpcd", "qwsxrq", "xqtcyy", "jhpgwcrq", "xqlx", "sxpt", "sxxt", "sxzxt", "sxmk", "sxsl", "yyghbz", "sjbb", "xtjgyxfx"];
    var sql = "update tlk_fzh_main_form set item_search_tmbh = ''";
    for (var i = 1; i < NEED_CLEAR_BOX.length; i++) {
        if ("jhpgwcrq".equals(NEED_CLEAR_BOX[i]) || "qwsxrq".equals(NEED_CLEAR_BOX[i])) {
            sql = sql + ", item_" + NEED_CLEAR_BOX[i] + " = null";
        } else {
            sql = sql + ", item_" + NEED_CLEAR_BOX[i] + " = ''";
        }
    }
    sql = sql + " where id = '" + doc.getId() + "'";
    updateByDSName(datasource, sql);
}

/**
 * 若不是4W1H项目，则清空
 * @param {*} doc 
 */
function clear4W1HBox(doc) {
    var box_4w1h = ["param2_what", "param1_what", "param3_where", "param2_where", "param1_where", "param3_who", "param2_who", "param1_who", "param2_when", "param1_when", "param3_how", "param2_how", "param1_how", "wh_what", "wh_where", "wh_who", "wh_when", "wh_how"];
    var ok = doc.getItemValueAsString("sywh");
    if ("否".equals(ok)) {
        var sql = "update tlk_fzh_clause_form set OPTIONITEM = '' ";
        for (var i = 0; i < box_4w1h.length; i++) {
            sql = sql + ", item_" + box_4w1h[i] + " = ''";
        }
        sql = sql + " where id = '" + doc.getId() + "'";
        updateByDSName(datasource, sql);
    }
}


/**
 * 获取当前文档（电子需求单）的负责人
 * @param {当前作用文档} doc 
 */
function getLeader(doc) {
    var leader = "";
    var qzxm = doc.getItemValueAsString("qzxm");
    if (isNotNull(qzxm)) {
        if ("是".equals(qzxm)) {
            leader = doc.getItemValueAsString("fzr");
        } else {
            var sys = doc.getItemValueAsString("ztxt_mc");
            sql = "select group_concat(item_tech) as item_data , '" + domainid + "' as domainid from tlk_system_manager  " +
                "where( " +
                "case when item_system_son is null or item_system_son = '' " +
                "then " +
                "item_system " +
                "else " +
                "concat(item_system, '-', item_system_son) " +
                "end " +
                ") in ( '" + sys + "' )";
            leader = findBySQL(sql).getItemValueAsString("data");
        }
    }
    return leader;
}


/**
 * 同步录入框展示框的内容
 * @param {文档} lrkBox
 * @param {文档表明} lrkBox
 * @param {录入框名} lrkBox 
 * @param {*展示框名} zskBox 
 * @param {*空值是否记录} nullIsOk 
 */
function synchroSpeakBox(doc, tableName, lrkBox, zskBox, nullIsOk) {
    var v = getItemValueAsString(lrkBox);
    if (nullIsOk || (!nullIsOk && isNotNull(v))) {
        v = (isNotNull(v) ? v : "");
        var user = getWebUser();
        var userName = user.getName();
        var v = userName + " : " + v + "             " + getCurDate("yyyy-MM-dd HH:mm");

        var sql_1 = "select '" + domainid + "' AS domainid, item_" + zskBox + " item_data from " + tableName + " where id = '" + doc.getId() + "'";
        var result = findBySQL(sql_1);
        var data = result.getItemValueAsString("data");
        if (isNotNull(data)) {
            v = data + "<br/>" + v;
        } else {
            v = "<br/>" + v;
        }
        var sql_2 = "update " + tableName + " set item_" + zskBox + " = '" + v + "', item_" + lrkBox + " = '' where id = '" + doc.getId() + "'";
        updateByDSName(datasource, sql_2);
    }
}