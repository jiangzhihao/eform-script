#include "bpmoa2";
#include "flow.util";
#include "flow.util.clause";
#include "flow.util.workload";

var ywlx = getItemValueAsString("ywlx");
var stateNow = getStateLabel();
var doc = getCurrentDocument();
var flowstate = doc.getLastFlowOperation(); //t=80表示提交；t=81表示回退

var syn_name_array = ["项目负责人评审信息", "部门领导评审信息", "中心领导评审信息", "设计部门项目经理判断", "开发公司接受情况"];
var syn_lrk_array = ['xmfzr_lrk', 'bmld_lrk', 'zxld_lrk', 'jskfb_lrk', 'jsqk_lrk'];
var syn_zsk_array = ['xmfzr_zsk', 'bmld_zsk', 'zxld_zsk', 'jskfb_zsk', 'jsqk_zsk'];
var syn_temp_box = ['yj', 'bmld_yj', 'zxld_yj', 'jskfb_yj', 'jsqk_yj'];

clearSplitBox(getCurrentDocument());

if (flow_state.FLOW_5.equals(stateNow) && flowstate == 81) { //返回到工作量评估
    //deleteWorkLoadTab();
}

//更新工作量
if ("常规完善类".equals(ywlx)) {
    //同步设计部门经理的评审意见
    if (flow_state.FLOW_6.equals(stateNow) && flowstate == 81) { //返回到 签字确认（项目经理）
        updateWorkload(WorkLoad_Type.type4);
    } else if (flow_state.FLOW_7.equals(stateNow) && flowstate == 81) { //返回到 签字确认（项目经理）
        updateWorkload(WorkLoad_Type.type3);
    }
}

//回退到条目拆分之前的话需要删除已经自动同步的部门负责人
if ((flow_state.FLOW_1.equals(stateNow) || flow_state.FLOW_2.equals(stateNow) || flow_state.FLOW_3.equals(stateNow) || flow_state.FLOW_4.equals(stateNow)) && flowstate == 81) {
    var sql_01 = "update tlk_setuo_carryout set item_xqtcbm_pgry = '', item_sjbm_pgry = '', item_kfgs_pgry = '', item_ywbm_pgry = '' where id = '" + doc.getId() + "'";
    updateByDSName(datasource, sql);
}

//同步信息
//1. 
if (flow_state.FLOW_3.equals(stateNow) && flowstate == 80) {
    var now = getToday();
    var value = "    " + getLastApproverName() + "         确认时间：" + getYear(now) + "-" + getMonth(now) + "-" + getDay(now);
    var sql = "update tlk_setuo_carryout set item_tcmnldqr = '" + value + "' where id = '" + doc.getId() + "'";
    updateByDSName(datasource, sql);
}
//有条目拆分返回后 或者 调整需求信息 需要清空负责人选择框
if ((flow_state.FLOW_4.equals(stateNow) && flowstate == 81) || (flow_state.FLOW_15.equals(stateNow) && flowstate == 80)) {
    var sql = "update tlk_setuo_carryout set item_dzxqdfzr = '' where id = '" + doc.getId() + "'";
    updateByDSName(datasource, sql);
}
//同步主配系统信息
if ((flow_state.FLOW_4.equals(stateNow) || flow_state.FLOW_5.equals(stateNow)) && flowstate == 80) {
    var v1 = getItemValueAsString("phxt_mc_ls");
    var v2 = getItemValueAsString("phxt_mc");
    if (isNotNull(v1)) {
        if (isNotNull(v2)) {
            v2 = v2 + ";" + v1;
        } else {
            v2 = v1;
        }
        v1 = "";
        var sqlll = "update tlk_setuo_carryout set item_phxt_mc_ls = '', item_phxt_mc = '" + v2 + "' where id = '" + doc.getId() + "'";
        updateByDSName(datasource, sqlll);
    }
    //同步录入框展示框
    synchro(syn_name_array[3], syn_temp_box[3], syn_zsk_array[3], syn_lrk_array[3], getCurrentDocument());
}

if (flow_state.FLOW_6.equals(stateNow) && flowstate == 80) {
    //code
    writeBackSignVersion(getCurrentDocument());
}

//3. 
if (((flow_state.FLOW_7.equals(stateNow) || flow_state.FLOW_9.equals(stateNow)) && flowstate == 80) || (flow_state.FLOW_6.equals(stateNow) && flowstate == 81)) {
    synchro(syn_name_array[0], syn_temp_box[0], syn_zsk_array[0], syn_lrk_array[0], getCurrentDocument());
}
//4. 
if (((flow_state.FLOW_9.equals(stateNow) || flow_state.FLOW_10.equals(stateNow)) && flowstate == 80) || (flow_state.FLOW_6.equals(stateNow) && flowstate == 81)) {
    synchro(syn_name_array[1], syn_temp_box[1], syn_zsk_array[1], syn_lrk_array[1], getCurrentDocument());
}
//5. 
if (((flow_state.FLOW_11.equals(stateNow) || flow_state.FLOW_12.equals(stateNow)) && flowstate == 80) || (flow_state.FLOW_6.equals(stateNow) && flowstate == 81)) {
    synchro(syn_name_array[2], syn_temp_box[2], syn_zsk_array[2], syn_lrk_array[2], getCurrentDocument());
}

//6. 
if (((flow_state.FLOW_12.equals(stateNow) || flow_state.FLOW_13.equals(stateNow)) && flowstate == 80)) {
    synchro(syn_name_array[4], syn_temp_box[4], syn_zsk_array[4], syn_lrk_array[4], getCurrentDocument());
}

if (null != stateNow && !flow_state.FLOW_14.equals(stateNow)) {
    var reqname = getCurrentDocument().getItemValueAsString("sqdh");
    var flowname = "电子需求单流程";
    if (!isNotNull(reqname)) {
        println("【生成OA待办】：获取reqname失败，时间：" + getToday() + "，采取紧急方法如下：");
        reqname = getCurrentDocument().getItemValueAsString("sqdh");
        println("reqname重新赋值：reqname=" + reqname);
    }
    bpmOa2(reqname, flowname);
}

/**
 * 同步评审意见
 */
function synchro(synName, tempBox, zskName, lrkName, doc) {
    var v = getItemValueAsString(tempBox);
    if (isNotNull(v) || !"".equals(v)) {
        var sql_1 = "select '" + domainid + "' AS domainid, item_" + zskName + " item_data from tlk_setuo_carryout where id = '" + doc.getId() + "'";
        var result = findBySQL(sql_1);
        var data = result.getItemValueAsString("data");
        if (isNotNull(data)) {
            v = data + "<br/>" + v;
        }
        var sql_2 = "update tlk_setuo_carryout set item_" + zskName + " = '" + v + "', item_" + tempBox + " = '' where id = '" + doc.getId() + "'";
        updateByDSName(datasource, sql_2);
    }
    var sql_3 = "update tlk_setuo_carryout set item_" + lrkName + " = '', item_" + tempBox + " = '' where id = '" + doc.getId() + "'";
    updateByDSName(datasource, sql_3);
}

/**
 * 条目拆分保存、提交之前，清空 条件查询、批量修改 字段
 */
function clearSplitBox(doc) { //
    var NEED_CLEAR_BOX = ["tmbh", "tmzt", "sxxt", "sxzxt", "jhbb", "xqbm_fzr", "ms", "sjbm_fzr", "kfgs_fzr", "tcbm", "xqbm_fzr_1", "sjbm_fzr_1", "kfgs_fzr_1", "jpcd", "qwsxrq", "xqtcyy", "jhpgwcrq", "xqlx", "sxpt", "sxxt_1", "sxzxt_1", "sxmk", "sxsl", "yyghbz", "sjbb", "xtjgyxfx"];
    var sql = "update tlk_setuo_carryout set item_tmbh = ''";
    for (var i = 1; i < NEED_CLEAR_BOX.length; i++) {
        if ("jhpgwcrq".equals(NEED_CLEAR_BOX[i]) || "qwsxrq".equals(NEED_CLEAR_BOX[i])) {
            sql = sql + ", item_" + NEED_CLEAR_BOX[i] + " = null";
        } else {
            sql = sql + ", item_" + NEED_CLEAR_BOX[i] + " = ''";
        }
    }
    sql = sql + " where id = '" + doc.getId() + "'";
    updateByDSName(datasource, sql);
}
