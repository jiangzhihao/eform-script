var datasource = "cpmis_ds";

var PRINT_NAME = "flow.util.workload";

/**
 * 初始化工作量评估Tab页
 * var targetForm_1 = "workload_assess_info";
   var targetForm_2 = "project_scheme";
 */
function initWorkLoadTab(parentId) {
    var docProcess = getDocumentProcess();
    var formProcess = getFormProcess();
    var domainid = getDomainid();
    var newForm1 = formProcess.doViewByFormName(Form_Name.WORK_LOAD, getApplication());
    var newForm2 = formProcess.doViewByFormName(Form_Name.PROJECT_PLAN, getApplication());
    var newForm3 = formProcess.doViewByFormName(Form_Name.CLAUSE_THIN, getApplication());
    if (isNotNull(parentId)) {
        initWworkLoad(domainid, parentId, newForm1, docProcess);
        initProjectPlan(domainid, parentId, newForm2, docProcess);
        initThinClause(domainid, parentId, newForm3, docProcess)
    }
}

/**
 * 生成工作量评估
 */
function initWworkLoad(domainid, parentId, newForm, docProcess) {
    //1. 查询该 立项与实施 表单关联的系统版本
    var sql = "SELECT DISTINCT '" + domainid + "' as domainid" +
        " , IFNULL(CONCAT(t1.item_xqsxxt, '-', t1.item_xqzxt, t1.item_dqbb)" +
        " , CONCAT(t1.item_xqsxxt,t1.item_dqbb)) AS item_sys_ver" +
        " , item_xqsxxt as item_xqsxxt" /* 实现系统 */ +
        " , item_xqzxt as item_xqzxt" /* 实现子系统 */ +
        " , item_dqbb as item_dqbb" /* 当前版本 */ +
        " FROM tlk_clause_present t1" +
        " where parent = '" + parentId + "'";
    var dataArray = queryBySQL(sql);
    //2. 为每一个系统版本创建一个对应的工作量评估
    if (null != dataArray && dataArray.size() > 0) {
        for (var j = 0; j < dataArray.size(); j++) {
            var data = dataArray.get(j);
            var version = data.getItemValueAsString("sys_ver");
            var xqsxxt = data.getItemValueAsString("xqsxxt"); //实现系统
            var xqzxt = data.getItemValueAsString("xqzxt"); //实现子系统
            var dqbb = data.getItemValueAsString("dqbb"); //系统版本

            var newDoc = docProcess.doNew(newForm, getWebUser(), createParamsTable());
            newDoc.setParent(parentId);
            newDoc.setAuthor(getWebUser().getId());
            newDoc.setIstmp(false);
            newDoc.setApplicationid(getApplication());
            newDoc.setDomainid(getWebUser().getDomainid());
            newDoc.addStringItem("xtbb", version);
            newDoc.addStringItem("fkzt", "开发未反馈");

            docProcess.doCreate(newDoc);
        }
    }


}

/**
 * 创建项目安排
 */
function initProjectPlan(domainid, parentId, newForm, docProcess) {
    //1. 查询该 立项与实施 表单关联的系统版本
    var sql = "SELECT DISTINCT '" + domainid + "' AS domainid" +
        " ,IFNULL(CONCAT(t1.item_xqsxxt, '-', t1.item_xqzxt, t1.item_dqbb)" +
        " ,CONCAT(t1.item_xqsxxt,t1.item_dqbb)) AS item_sys_ver" +
        " ,t1.item_xqsxxt AS item_sys" +
        " ,t1.item_xqzxt AS item_sys_son" +
        " ,t1.item_dqbb AS item_ver" +
        " FROM tlk_clause_present t1" +
        " where parent = '" + parentId + "'";
    var dataList = queryBySQL(sql);
    //2. 为每一个系统版本创建一个项目安排记录
    if (null != dataList && dataList.size() > 0) {
        for (var j = 0; j < dataList.size(); j++) {
            var data = dataList.get(j);
            var version = data.getItemValueAsString("sys_ver");
            var xqsxxt = data.getItemValueAsString("sys"); //实现系统
            var xqzxt = data.getItemValueAsString("sys_son"); //实现子系统
            var dqbb = data.getItemValueAsString("ver"); //系统版本

            var sqll = "select '" + domainid + "' AS domainid, group_concat(item_zt separator '==') as item_zt from tlk_clause_present t1 where " +
                "IFNULL(CONCAT(t1.item_xqsxxt, '-', t1.item_xqzxt, t1.item_dqbb) " +
                " ,CONCAT(t1.item_xqsxxt,t1.item_dqbb)) = '" + version + "'"
            var res = findBySQL(sqll);
            var ztStr = res.getItemValueAsString("zt");
            var array = splitText(ztStr, "==");
            var xqsx = "";
            for (var t = 0; t < array.length; t++) {
                xqsx = xqsx + (t + 1) + ". " + array[t] + "    <br/>"
            }
            var newDoc = docProcess.doNew(newForm, getWebUser(), createParamsTable());
            newDoc.setParent(parentId);
            newDoc.setAuthor(getWebUser().getId());
            newDoc.setIstmp(false);
            newDoc.setApplicationid(getApplication());
            newDoc.setDomainid(getWebUser().getDomainid());
            newDoc.addStringItem("shixianxitong", xqsxxt);
            newDoc.addStringItem("shixianzixitong", xqzxt);
            newDoc.addStringItem("shangxianbanben", dqbb);
            newDoc.addStringItem("xuqiushixianneirong", xqsx);
            docProcess.doCreate(newDoc);
        }
    }
}


/**
 * 初始化细化条目
 */
function initThinClause(domainid, parentId, newForm, docProcess) {
    var sql = "select DISTINCT '" + domainid + "' AS domainid, id as item_id, item_tmbh as item_tmbh, item_zt as item_zt, item_xqsxxt as item_xqsxxt, item_xqzxt as item_xqzxt, item_dqbb as item_dqbb " +
        " , IFNULL( " +
        " CONCAT(t1.item_xqsxxt, '-', t1.item_xqzxt, t1.item_dqbb) " +
        " ,CONCAT(t1.item_xqsxxt,t1.item_dqbb) " +
        " ) AS item_sys_ver " +
        " , CONCAT( t1.item_tmbh, '_', t1.item_zt) as item_title "
        //+ " , 'HAHAHAHA' as item_title "
        +
        " from tlk_clause_present t1 where parent = '" + parentId + "'"
    var dataList = queryBySQL(sql);
    if (null != dataList && dataList.size() > 0) {
        for (var j = 0; j < dataList.size(); j++) {
            var data = dataList.get(j);
            var tm_id = data.getItemValueAsString("id");
            var title = data.getItemValueAsString("title");
            var version = data.getItemValueAsString("sys_ver");
            var tmbh = data.getItemValueAsString("tmbh");
            var zt = data.getItemValueAsString("zt");
            var xt = data.getItemValueAsString("xqsxxt");
            var zxt = data.getItemValueAsString("xqzxt");
            var bb = data.getItemValueAsString("dqbb");
            var newDoc = docProcess.doNew(newForm, getWebUser(), createParamsTable());
            var sqll = "select '" + domainid + "' as domainid, id as item_id from tlk_workload_assess_info where item_xtbb = '" + version + "' and parent = '" + parentId + "'";
            var ress = findBySQL(sqll);
            var id = ress.getItemValueAsString("id");
            newDoc.setParent(id);
            newDoc.setAuthor(getWebUser().getId());
            newDoc.setIstmp(false);
            newDoc.setApplicationid(getApplication());
            newDoc.setDomainid(getWebUser().getDomainid());
            newDoc.addStringItem("xhtm_xqtm", title); //对应的条目标题
            newDoc.addStringItem("xhtm_xtbb", version); //对应的系统版本
            newDoc.addStringItem("xhtm_tmid", tm_id); //对应的条目ID
            newDoc.addStringItem("tm_bh", tmbh); //对应的条目编号
            newDoc.addStringItem("tm_zt", zt); //对应的条目主题
            newDoc.addStringItem("tm_xt", xt); //对应的条目主题
            newDoc.addStringItem("tm_zxt", zxt); //对应的条目主题
            newDoc.addStringItem("tm_bb", bb); //对应的条目主题
            docProcess.doCreate(newDoc);
        }
    } else {}
}





/**
 * 更新工作量
 * @param type：更新类型（来自flow.util）
 * @param v：更新的值，预算来源修改的值
 * @param bt：立项对应的预算来源，也就是要更新的预算
 */
function updateWorlloadHasValue(type, v, bt) {
    var yyl = "";
    var kyl = "";
    var djl = "";
    if (WorkLoad_Type.type4.equals(type)) {
        kyl = "+" + v;
        djl = "-" + v;
    } else if (WorkLoad_Type.type5.equals(type)) {
        yyl = "-" + v;
        kyl = "+" + v;
    } else if (WorkLoad_Type.type2.equals(type)) {
        djl = "+" + v;
        yyl = "-" + v;
    } else if (WorkLoad_Type.type6.equals(type)) {
        djl = "-" + v;
        yyl = "+" + v;
    }
    var sql = "update tlk_budget_management set item_kyl = item_kyl" + kyl + ", item_djl = item_djl" + djl + ", item_yyl = item_yyl" + yyl + " where item_bt = '" + bt + "'"; //ysly
    updateByDSName(datasource, sql);
}


/**
 * 更新工作量数据
 * 用于在操作一个具体立项流程的时候
 */
function updateWorkload(type) {
    var doc = getCurrentDocument();
    if (isNotNull(type)) {
        var domainid = getDomainid();
        var sql_1 = "select sum(item_zhgzl_zs) as item_total, '" + domainid + "'  as domainid from tlk_workload_assess_info where parent = '" + doc.getId() + "'";
        var result = findBySQL(sql_1);
        var v = result.getItemValueAsDouble("total");
        //预算管理信息
        var ysgl = getItemValueAsString();
        var sql_2 = "select bm.id as item_id, bm.item_kyl as item_kyl, bm.item_djl as item_djl, bm.item_yyl as item_yyl, '" + domainid + "'  as domainid  " +
            "from tlk_budget_management bm " +
            "left join tlk_setuo_carryout st " +
            "on bm.ITEM_BT = st.item_ysly " +
            "where st.id = '" + doc.getId() + "'";
        var result = findBySQL(sql_2);
        if (isNotNull(result)) {
            var id = result.getItemValueAsString("id");
            var kyl = result.getItemValueAsDouble("kyl");
            var djl = result.getItemValueAsDouble("djl");
            var yyl = result.getItemValueAsDouble("yyl");
            if (WorkLoad_Type.type1.equals(type)) { //增加冻结量，减少可用量
                djl = djl + v;
                kyl = kyl - v;
            } else if (WorkLoad_Type.type2.equals(type)) { //增加已用量，减少冻结量
                djl = djl - v;
                yyl = yyl + v;
            } else if (WorkLoad_Type.type3.equals(type)) { //增加冻结量，减少已用量
                djl = djl + v;
                yyl = yyl - v;
            } else if (WorkLoad_Type.type4.equals(type)) { //增加可用量， 減少冻结量
                kyl = kyl + v;
                djl = djl - v;
            } else if (WorkLoad_Type.type5.equals(type)) {
                yyl = yyl - v;
                kyl = kyl + v;
            }
            var sql_3 = "update tlk_budget_management set item_kyl = " + kyl + ", item_djl = " + djl + ", item_yyl = " + yyl + " where id = '" + id + "'";
            updateByDSName(datasource, sql_3);
        }
    }
}


function deleteWorkLoadTab() {
    var doc = getCurrentDocument();
    var docId = doc.getId();

    var sql_1 = "delete from tlk_clause_thin where parent in ( " +
        " select wa.id from tlk_workload_assess_info wa left join tlk_setuo_carryout sc on sc.id = wa.parent " +
        " where sc.id = '" + docId + "')";
    var sql_2 = "delete from tlk_workload_assess_info where parent = '" + docId + "'";
    var sql_3 = "delete from tlk_project_scheme where parent = '" + docId + "'";

    deleteByDSName(datasource, sql_1);
    deleteByDSName(datasource, sql_2);
    deleteByDSName(datasource, sql_3);
}