/* 表单的校验脚本 */

/**
 * 提示信息
 */
var Project_Process = {
    P_1: '需求分析',
    P_2: '开发',
    P_3: '验收测试',
    P_4: '模拟测试',
    P_5: '上线',
    P_1_MESSAGE: '【需求分析】未填写',
    P_2_MESSAGE: '【开发】未填写',
    P_3_MESSAGE: '【验收测试】未填写',
    P_4_MESSAGE: '【模拟测试】未填写',
    P_5_MESSAGE: '【上线】未填写',
    P_0_MESSAGE: '已完成'
}

/**
 * 返回值
 */
var ResultMessage = {
    createSuccessResult: function() {
        var Result = {
            result: true,
            message: ""
        };
        return Result;
    },
    createErrorResult: function(message_) {
        if (!isNotNull(message_)) {
            message_ = "操作失败";
        }
        var Result = {
            result: false,
            message: message_
        }
        return Result;
    }
}

/**
 * 条目拆分节点调整评估人员校验
 * @param {*} notNullBoxes  不可以为空的评估人员文本框
 * @param {*} boxName 对应的评估人员名称
 */
function editAuditors(notNullBoxes, boxName) {
    var errorMsg = "";
    var result = true;
    if (null != notNullBoxes && null != boxName && notNullBoxes.length == boxName.length) {
        for (var i = 0; i < notNullBoxes.length; i++) {
            var str = getItemValueAsString(notNullBoxes[i]);
            if (!isNotNull(str)) {
                if (isNotNull(errorMsg)) {
                    errorMsg = errorMsg + "、" + boxName[i];
                } else {
                    errorMsg = boxName[i];
                }
                result = false;
            }
        }
        if (result) {
            return ResultMessage.createSuccessResult();
        } else {
            return ResultMessage.createErrorResult(errorMsg + "不能为空");
        }
    } else {
        return ResultMessage.createSuccessResult();
    }
}

//是否可以创建子表单对象
function accessCreateSonForm(parentName, parentTableName) {
    var domainid = getDomainid();
    var pDoc = getParentDocument();
    var sql = "select '" + domainid + "' as domainid, count(1) as item_count from " + parentTableName + " where id = '" + pDoc.getId() + "'";
    var result = findBySQL(sql);
    var count = result.getItemValueAsString("count");
    if (!(null != count && "1".equals(count))) {
        return ResultMessage.createErrorResult("请先保存父表单-" + parentName);
    } else {
        return ResultMessage.createSuccessResult();
    }
}


/**
 * 设计部门项目经理选择主配系统
 */
function selectSystem() {
    var value = getItemValueAsString("ztxt_mc");
    if (!isNotNull(value)) {
        return ResultMessage.createErrorResult("请选择主配系统");
    } else {
        return ResultMessage.createSuccessResult();
    }
}

/**
 * 条目拆分提交校验
 * @param {*} doc 
 */
function clauseAccess(doc) {
    //1. 条目数量
    var childs = doc.getChilds("clause_present");
    if (null == childs || childs.size() <= 0) {
        return ResultMessage.createErrorResult("条目数量不能为0");
    } else {
        //条目评估是否完成
        var sql =
            "select '" + domainid + "' AS domainid" +
            ", ( " +
            " select count(1) from tlk_clause_present where " +
            "(item_jhpgwcsj is null or item_jhpgwcsj = ''  " +
            " or item_xqlx is  null or item_xqlx = '' " +
            " or item_sxpt is  null or item_sxpt = '' " +
            " or item_xqsxxt is  null or item_xqsxxt = '' " +
            " or item_xqsxsl is  null or item_xqsxsl = '' " +
            " or item_jhsxbb is  null or item_jhsxbb = '' " +
            " or item_dqbb is  null or item_dqbb = '' " +
            " or item_jpcd is  null or item_jpcd = ''  " +
            " or item_qwsxsj is  null or item_qwsxsj = '' " +
            " or item_tcyy is  null or item_tcyy = '' " +
            " or item_xqbmfzr is  null or item_xqbmfzr = '' " +
            " )" +
            " and parent = '" + doc.getId() + "' " +
            " ) as item_count " +
            " from dual";
        var result = findBySQL(sql);
        var sjCount = result.getItemValueAsString("count");
        if (null != sjCount && sjCount != "0") {
            return ResultMessage.createErrorResult("条目评估未完成");
        } else {
            //条目所选系统是否合法
            var sql_1 = "select '" + domainid + "' AS domainid, ( " +
                " case when (ITEM_phxt_mc is null or ITEM_phxt_mc = '')" +
                " then ITEM_ztxt_mc" +
                " else concat(ITEM_ztxt_mc, ''',''', replace(ITEM_phxt_mc, ';', ''',''')) end)" +
                " as item_data  from tlk_setuo_carryout where id = '" + doc.getId() + "'"
            var data_1 = findBySQL(sql_1).getItemValueAsString("data");
            var sql_2 = "select '" + domainid + "' AS domainid, count(1) as item_count from tlk_clause_present where parent = '" + doc.getId() + "' and " +
                " ( " +
                " case when ITEM_XQZXT is null or ITEM_XQZXT = '' " +
                " then ITEM_XQSXXT " +
                " else concat(ITEM_XQSXXT, '_', ITEM_XQZXT) end " +
                " ) not in ('" + data_1 + "')";
            var data_2 = findBySQL(sql_2).getItemValueAsString("count");
            if (null == data_2 || !"0".equals(data_2)) {
                return ResultMessage.createErrorResult("存在条目所属系统不在所选主配系统中");
            } else {
                var vBox1 = getItemValueAsString("xqtcbm_pgry");
                var vBox2 = getItemValueAsString("sjbm_pgry");
                var vBox3 = getItemValueAsString("kfgs_pgry");
                var vBox4 = getItemValueAsString("ywbm_pgry");
                if (!isNotNull(vBox1) && !isNotNull(vBox2) && !isNotNull(vBox3) && !isNotNull(vBox4)) {
                    return ResultMessage.createErrorResult("评估人员不能全部为空");
                } else {
                    var vBox5 = getItemValueAsString("dzxqdfzr");
                    var sql_pgry = "select '" + domainid + "' as domainid, item_dzxqdfzr, item_xqtcbm_pgry , item_sjbm_pgry , item_kfgs_pgry , item_ywbm_pgry  from tlk_setuo_carryout where id = '" + getCurrentDocument().getId() + "'";
                    var result_pgry = findBySQL(sql_pgry);
                    if (null != result_pgry) {
                        var v1 = result_pgry.getItemValueAsString("xqtcbm_pgry");
                        var v2 = result_pgry.getItemValueAsString("sjbm_pgry");
                        var v3 = result_pgry.getItemValueAsString("kfgs_pgry");
                        var v4 = result_pgry.getItemValueAsString("ywbm_pgry");
                        var v5 = result_pgry.getItemValueAsString("dzxqdfzr");
                        if (!v1.equals(vBox1) || !v2.equals(vBox2) || !v3.equals(vBox3) || !v4.equals(vBox4) || !v5.equals(vBox5)) {
                            return ResultMessage.createErrorResult("评估人员已经修改，请保存后再提交");
                        }
                    }
                }
            }
        }
    }
    return ResultMessage.createSuccessResult();
}

/**
 * 工作量评估校验
 *
 * @param doc
 *
 * @Description:
 * 1. 工作量评估是否完成
 * 2. 预算工作量是否超出总量
 * 3. 期数是否填写完整
 * 4. 进度反馈情况
 */
function workloadAccess(doc) {
    var domainid = getDomainid();
    //工作量评估是否完成
    var childs1 = doc.getChilds("workload_assess_info");
    if (null != childs1 && childs1.size() > 0) {
        for (var t = 0; t < childs1.size(); t++) {
            var child = childs1.get(t);
            var state = child.getItemValueAsString("fkzt");
            if (!(null != state && "已完成".equals(state))) {
                return ResultMessage.createErrorResult("评估信息未完成");
            }
        }
    }
    childs1 = doc.getChilds("workload_assess_info");
    //监测进度填写情况
    for (var i = 0; i < childs1.size(); i++) {
        var child = childs1.get(i);
        var xtbb = child.getItemValueAsString("xtbb");
        var sql_2 = "select '" + domainid + "' as domainid, id as item_id from tlk_project_ver_plan where " +
            " ( " +
            " case when (item_sysnam_son is null or item_sysnam_son = '') " +
            " then concat(item_system, item_plan_ver) " +
            " else concat(item_system, '-', item_sysnam_son, item_plan_ver) end " +
            " )   = '" + xtbb + "' ";
        var result_2 = findBySQL(sql_2);
        if (isNotNull(result_2)) {
            var value = "";
            var pid = result_2.getItemValueAsString("id");
            var sql_1 = "" +
                " select '" + domainid + "' as domainid, ( " +
                " case when ( " +
                " (select count(1) from tlk_project_plan where parent = '" + pid + "' " +
                " and item_stage = '" + Project_Process.P_1 + "' and (item_plan_start is null or item_plan_start = '')) = 1 " +
                " ) " +
                " then '" + Project_Process.P_1_MESSAGE + "' "

            +" when ( " +
            " (select count(1) from tlk_project_plan where parent = '" + pid + "'  " +
                " and item_stage = '" + Project_Process.P_2 + "' and (item_plan_start is null or item_plan_start = '')) = 1 " +
                " ) " +
                " then '" + Project_Process.P_2_MESSAGE + "' "

            +
            " when ( " +
            " (select count(1) from tlk_project_plan where parent = '" + pid + "'  " +
                " and item_stage = '" + Project_Process.P_3 + "' and (item_plan_start is null or item_plan_start = '')) = 1 " +
                " ) " +
                " then '" + Project_Process.P_3_MESSAGE + "' "

            +
            " when ( " +
            " (select count(1) from tlk_project_plan where parent = '" + pid + "'  " +
                " and item_stage = '" + Project_Process.P_4 + "' and (item_plan_start is null or item_plan_start = '')) = 1 " +
                " ) " +
                " then '" + Project_Process.P_4_MESSAGE + "' "

            +
            " when ( " +
            " (select count(1) from tlk_project_plan where parent = '" + pid + "'  " +
                " and item_stage = '" + Project_Process.P_5 + "' and (item_plan_start is null or item_plan_start = '')) = 1 " +
                " ) " +
                " then '" + Project_Process.P_5_MESSAGE + "' "

            +
            " else '" + Project_Process.P_0_MESSAGE + "' end ) as item_result " +
                " from dual ";
            var result_1 = findBySQL(sql_1);
            if (null != result_1) {
                value = result_1.getItemValueAsString("result");
            }
            if (!(null != value && value.equals(Project_Process.P_0_MESSAGE))) {
                return ResultMessage.createErrorResult("进度填写未完成");
            }

        }
    }

    //期数是否填写完整
    var childs2 = doc.getChilds("project_scheme");
    var projects = "";
    if (null != childs2 && childs2.size() > 0) {
        for (var n = 0; n < childs2.size(); n++) {
            var child = childs2.get(n);
            var qs = child.getItemValueAsString("qishu");
            if (!isNotNull(qs)) {
                var system = child.getItemValueAsString("shixianxitong");
                var son = child.getItemValueAsString("shixianzixitong");
                var version = child.getItemValueAsString("shangxianbanben");
                var msg = (isNotNull(son)) ? (system + "-" + son + version) : (system + version);
                projects = (isNotNull(projects)) ? (projects + "、" + msg) : (msg);
            }
        }
    }
    if (isNotNull(projects)) {
        return ResultMessage.createErrorResult("项目整体安排计划需要填写期数");
    }
    //预算工作量是否超出总量
    var ywlx = doc.getItemValueAsString("ywlx");
    if ("常规完善类".equals(ywlx)) {
        var v1 = doc.getItemValueAsDouble("bclxgzl");
        var v2 = doc.getItemValueAsDouble("kyl");
        if (v1 > v2) {
            return ResultMessage.createErrorResult("工作量超出可用值");
        }
    }
    return ResultMessage.createSuccessResult();
}