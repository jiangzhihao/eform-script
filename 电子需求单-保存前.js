#include "flow.util";
#include "flow.util.validate";

var doc = getCurrentDocument();
var message = "";
var stateNow = getStateLabel();

if (null == stateNow || flow_state.FLOW_1.equals(stateNow) || flow_state.FLOW_0.equals(stateNow) || flow_state.FLOW_15.equals(stateNow)) {
    var v = getItemValueAsString("ywlx");
    if ("新业务类".equals(v)) {
        getCurrentDocument().findItem("ysly").setValue("");
        getCurrentDocument().findItem("ysssqdqbbh").setValue("");
    }
}

if (flow_state.FLOW_4.equals(stateNow)) {
    var result = selectSystem();
    if (result.result) {
        var doc = getCurrentDocument();
        var v1 = getItemValueAsString("phxt_mc");
        var v2 = getItemValueAsString("phxt_mc_ls");

        if (isNotNull(v2)) {
            if (isNotNull(v1)) {
                v1 = v1 + ";" + v2;
            } else {
                v1 = v2;
            }
        }
        v2 = "";
        doc.findItem("phxt_mc").setValue(v1);
        doc.findItem("phxt_mc_ls").setValue(v2);
    } else {
        message = result.message;
    }
}

if (flow_state.FLOW_5.equals(stateNow)) {
    var auditorBoxes = ["xqtcbm_pgry", "sjbm_pgry", "kfgs_pgry", "ywbm_pgry", "dzxqdfzr"];
    var auditorNotNullBoxes = ["xqtcbm_pgry", "sjbm_pgry", "kfgs_pgry", "ywbm_pgry", "dzxqdfzr"];
    var auditorNotNullBoxesName = ["需求提出部门评估人员", "设计部门评估人员", "开发公司评估人员", "运维部门评估人员", "电子需求单负责人"];
    var result1 = editAuditors(auditorNotNullBoxes, auditorNotNullBoxesName);
    if (!result1.result) {
        message = result1.message
    }
}
message;