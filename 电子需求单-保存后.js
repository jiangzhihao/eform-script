#include "flow.util";

var stateNow = getStateLabel();

clearSplitBox(getCurrentDocument());

if (flow_state.FLOW_5.equals(stateNow)) {
    var auditorBoxes = ["xqtcbm_pgry", "sjbm_pgry", "kfgs_pgry", "ywbm_pgry", "dzxqdfzr"];
    var auditorNotNullBoxes = ["xqtcbm_pgry", "sjbm_pgry", "kfgs_pgry", "ywbm_pgry", "dzxqdfzr"];
    var auditorNotNullBoxesName = ["需求提出部门评估人员", "设计部门评估人员", "开发公司评估人员", "运维部门评估人员", "电子需求单负责人"];
    editAuditors(auditorBoxes, auditorNotNullBoxes, auditorNotNullBoxesName)
}


/**
 * 条目拆分保存、提交之前，清空 条件查询、批量修改 字段
 */
function clearSplitBox(doc) { //
    var NEED_CLEAR_BOX = ["tmbh", "tmzt", "sxxt", "sxzxt", "jhbb", "xqbm_fzr", "ms", "sjbm_fzr", "kfgs_fzr", "tcbm", "xqbm_fzr_1", "sjbm_fzr_1", "kfgs_fzr_1", "jpcd", "qwsxrq", "xqtcyy", "jhpgwcrq", "xqlx", "sxpt", "sxxt_1", "sxzxt_1", "sxmk", "sxsl", "yyghbz", "sjbb", "xtjgyxfx"];
    var sql = "update tlk_setuo_carryout set item_tmbh = ''";
    for (var i = 1; i < NEED_CLEAR_BOX.length; i++) {
        if ("jhpgwcrq".equals(NEED_CLEAR_BOX[i]) || "qwsxrq".equals(NEED_CLEAR_BOX[i])) {
            sql = sql + ", item_" + NEED_CLEAR_BOX[i] + " = null";
        } else {
            sql = sql + ", item_" + NEED_CLEAR_BOX[i] + " = ''";
        }
    }
    sql = sql + " where id = '" + doc.getId() + "'";
    updateByDSName(datasource, sql);
}





/**
 * 修改审批人
 * getCurrentDocument必须是电子需求单文档
 */
function editAuditors(auditorBoxes, auditorNotNullBoxes, auditorNotNullBoxesName) {

    var doc = getCurrentDocument();
    var process = getDocumentProcess();
    //获取新的审批人
    var userStr = doc.getItemValueAsString(auditorBoxes[0]);
    for (var i = 1; i < auditorBoxes.length; i++) {
        users = doc.getItemValueAsString(auditorBoxes[i]);
        if (isNotNull(users)) {
            userStr = userStr + ";" + users;
        }
    }
    //去重
    var set = createObject("java.util.HashSet");
    var array = splitText(userStr, ";");
    for (var i = 0; i < array.length; i++) {
        var a = array[i];
        if (isNotNull(a)) {
            set.add(a);
        }
    }
    //{"1524017217374":["yusen","xuxiaodong","I4028882134c4ef350151ccccf627066c"]}
    //组合成json字符串，同时获取param形式的id字符串
    var ids = "SSS";
    var n = 0;
    var jsonStr = '{"1524017217374":[';
    var iter = set.iterator();
    while (iter.hasNext()) {
        var u = iter.next();
        if (n > 0) {
            jsonStr = jsonStr + ',"' + u + '"';
        } else {
            jsonStr = jsonStr + '"' + u + '"';
        }
        n++;
        ids = ids + "','" + u;
    }
    jsonStr = jsonStr + ']}';
    //获取审批人对应的姓名
    var domainid = getDomainid();
    var sql = "select '" + domainid + "' as domainid, group_concat(name) as item_names from bpm.t_user where id in ('" + ids + "')";
    var res = findBySQL(sql);
    var names = "";
    if (null != res) {
        names = res.getItemValueAsString("names");
    }
    //更新
    doc.setAuditorList(jsonStr); // json格式
    doc.setAuditorNames(names);
    doc.setLastmodifier(getWebUser().getId());
    var params = createParamsTable();
    params.setParameter("_currid", "1524017217374");
    process.doChangeAuditor(doc, params, getWebUser());
}