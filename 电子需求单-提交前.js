#include "bpmoa";
#include "bpmoa3";
#include "flow.util"
#include "flow.util.workload";
#include "flow.util.validate"

var errorMsg = "";
var stateNow = getStateLabel();
var flowType = getParameter("_flowType");
var userId = getWebUser().getId();
var sendUserId = getItemValueAsString("send_user_id");
var currnodeid = getParameter("_nextids");
var targetSite = getCurrentDocument().getFlowVO().toFlowDiagram().getNodeByID(currnodeid).name;

//在提出需求、调整需求 两个节点可以切换业务类型，切换成新业务类的时候需要清空所选择的关联预算管理
if (null == stateNow || flow_state.FLOW_1.equals(stateNow) || flow_state.FLOW_0.equals(stateNow) || flow_state.FLOW_15.equals(stateNow)) {
    var v = getItemValueAsString("ywlx");
    if ("新业务类".equals(v)) {
        getCurrentDocument().findItem("ysly").setValue("");
        getCurrentDocument().findItem("ysssqdqbbh").setValue("");
    }
}

//设计部门项目经理
if (flow_state.FLOW_4.equals(stateNow)) {
    if ("80".equals(flowType)) {
        var result = selectSystem();
        if (result.result) {
            tbpsxx(getCurrentDocument(), 4)
            var doc = getCurrentDocument();
            var v1 = getItemValueAsString("phxt_mc");
            var v2 = getItemValueAsString("phxt_mc_ls");
            if (isNotNull(v2)) {
                if (isNotNull(v1)) {
                    v1 = v1 + ";" + v2;
                } else {
                    v1 = v2;
                }
            }
            v2 = "";
            doc.findItem("phxt_mc").setValue(v1);
            doc.findItem("phxt_mc_ls").setValue(v2);
        } else {
            errorMsg = result.message;
        }
    } else {
        tbpsxx(getCurrentDocument(), 4)
        var doc = getCurrentDocument();
        var v1 = getItemValueAsString("phxt_mc");
        var v2 = getItemValueAsString("phxt_mc_ls");
        if (isNotNull(v2)) {
            if (isNotNull(v1)) {
                v1 = v1 + ";" + v2;
            } else {
                v1 = v2;
            }
        }
        v2 = "";
        doc.findItem("phxt_mc").setValue(v1);
        doc.findItem("phxt_mc_ls").setValue(v2);
    }
}

//条目拆分之前进行校验
if (flow_state.FLOW_5.equals(stateNow) && "80".equals(flowType)) {
    var result = clauseAccess(getCurrentDocument());
    if (!result.result) {
        errorMsg = result.message;
    }
}

//在工作量评估 节点， 提交至实施（新业务类）和会签（常规类）的时候需要校验信息
//工作量评估 提交校验
if (flow_state.FLOW_6.equals(stateNow) && flowType.equals("80") &&
    (flow_state.FLOW_7.equals(targetSite) || flow_state.FLOW_12.equals(targetSite))) {
    var result = workloadAccess(getCurrentDocument());
    if (!result.result) {
        errorMsg = result.message;
    }
}

if (flow_state.FLOW_7.equals(stateNow)) {
    tbpsxx(getCurrentDocument(), 0)
}

if (flow_state.FLOW_9.equals(stateNow)) {
    tbpsxx(getCurrentDocument(), 1)
}

if (flow_state.FLOW_11.equals(stateNow)) {
    tbpsxx(getCurrentDocument(), 2)
}

if (flow_state.FLOW_12.equals(stateNow)) {
    tbpsxx(getCurrentDocument(), 3)
}

//-------------------------------------------调用OA已办接口-------------------------------------------//
if (!isNotNull(errorMsg)) {
    var reqname = getCurrentDocument().getItemValueAsString("sqdh");
    var flowname = "电子需求单流程";
    if (!isNotNull(reqname)) {
        println("【生成OA已办】：获取reqname失败，时间：" + getToday() + "，采取紧急方法如下：");
        reqname = getCurrentDocument().getItemValueAsString("sqdh");
        println("reqname重新赋值：reqname=" + reqname);
    }
    bpmOa(reqname, flowname);
    if ("80".equals(flowType) && (flow_state.FLOW_5.equals(stateNow) || flow_state.FLOW_6.equals(stateNow))) { //需要删除其他人的代办
        bpmOa3(reqname, flowname);
    } else if ("81".equals(flowType)) {
        bpmOa3(reqname, flowname);
    }
}

errorMsg;