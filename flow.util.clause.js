/**
 * 数据源
 */
var datasource = "cpmis_ds";

/**
 * 根据 主配系统 获取 系统相关负责人
 * @param manType：获取负责人类型（enum）
 *
 * 
 * 主体系统：[ztxt_mc]
 * 配合系统：[phxt_mc]
 * 注意：唯一一次同步的时候使用
 */
function getSystemMan(manType) {
    var sql = "";
    var nameStr = "";
    var connect_symbol = "_";
    //1. 获取主系统名称
    var mainSys = getItemValueAsString("ztxt_mc");
    nameStr = nameStr + mainSys;
    //2. 获取配合系统名称
    var viceSysNameArray = splitText(getItemValueAsString("phxt_mc"), ";");
    //3. 整合
    if ((null != viceSysNameArray) && (viceSysNameArray.length > 0)) {
        //code
        for (var i = 0; i < viceSysNameArray.length; i++) {
            //code
            nameStr += "','" + viceSysNameArray[i];
        }
    }
    var param = nameStr;
    var domainid = getDomainid();
    if (ManType.develop.equals(manType)) { //开发（开发+测试）
        //code
        sql = "SELECT concat(group_concat(item_dev),',', group_concat(item_test)) as item_data " +
            ",'" + domainid + "'  as domainid  from tlk_system_manager where " +
            "(case when (item_system_son is null or item_system_son = '')" +
            "then (item_system) else concat(item_system, '" + connect_symbol + "', item_system_son) end ) " +
            "in ( '" + param + "' )";
    } else if (ManType.develop_no_test.equals(manType)) { //仅开发（实施环节）
        sql = "SELECT group_concat(item_dev) as item_data " +
            ",'" + domainid + "'  as domainid  from tlk_system_manager where " +
            "(case when (item_system_son is null or item_system_son = '')" +
            "then (item_system) else concat(item_system, '" + connect_symbol + "', item_system_son) end ) " +
            "in ( '" + param + "' )";
    } else if (ManType.design.equals(manType)) { //设计部门
        //code
        sql = "SELECT group_concat(item_tech) as item_data, '" + domainid + "'  as domainid  from tlk_system_manager where " +
            "(case when (item_system_son is null or item_system_son = '')" +
            "then (item_system) else concat(item_system, '" + connect_symbol + "', item_system_son) end ) " +
            "in ( '" + param + "' )";
    } else if (ManType.opera.equals(manType)) { //运维
        sql = "SELECT group_concat(item_oper)  as item_data ,'" + domainid + "'  as domainid  from tlk_system_manager where " +
            "(case when (item_system_son is null or item_system_son = '')" +
            "then (item_system) else concat(item_system, '" + connect_symbol + "', item_system_son) end ) " +
            "in ( '" + param + "' )";
    } else if (ManType.demand.equals(manType)) { //需求部门
        sql = "SELECT concat(group_concat(item_qs),',', group_concat(item_info),',', group_concat(item_mark1),',', group_concat(item_mark2),',', group_concat(item_others))  as item_data ,'" + domainid + "'  as domainid  from tlk_system_manager where " +
            "(case when (item_system_son is null or item_system_son = '')" +
            "then (item_system) else concat(item_system, '" + connect_symbol + "', item_system_son) end ) " +
            "in ( '" + param + "' )";
    } else { //ALL或其他
        sql = "SELECT concat(group_concat(item_qs),',', group_concat(item_others), ',',  group_concat(item_mark2), ',', group_concat(item_mark1), ',', group_concat(item_info),',',group_concat(item_dev),',', group_concat(item_test),',', group_concat(item_tech),',', group_concat(item_oper))  as item_data ,'" + domainid + "'  as domainid  from tlk_system_manager where " +
            "(case when (item_system_son is null or item_system_son = '')" +
            "then (item_system) else concat(item_system, '" + connect_symbol + "', item_system_son) end ) " +
            "in ( '" + param + "' )";
    }
    var result = findBySQL(sql);
    var str = "";
    str = result.getItemValueAsString("data");
    return str;
}

/**
 * 数据去重
 * 参数不能为空，否则抛异常
 * @param str：param1,param2,...,paramn
 * @param symbol：原始连接符
 * @param new_symbol：新连接符
 */
function getDistinct(str, symbol, new_symbol) {
    var result = "";
    var targetSet = createObject("java.util.HashSet");
    var paramArray = splitText(str, symbol);
    for (var i = 0; i < paramArray.length; i++) {
        //code
        var param = paramArray[i];
        targetSet.add(param);
    }
    var n = targetSet.size();
    var num = 0;
    var i = targetSet.iterator();
    while (i.hasNext()) {
        //code
        if (num == n - 1) {
            //code
            result += i.next();
        } else {
            result += i.next() + new_symbol;
        }
        num++;
    }
    return result;
}

var ManType = {
    develop_no_test: 'developWithoutTest',
    develop: 'develop',
    design: 'design',
    opera: 'optera',
    demand: 'demand',
    all: 'all'
}

/**
 * 进入实施阶段后，将细化条目的工作量反馈回一级条目中
 * 作用在：流程图两条进入实施的路径
 */
function feedbackManpower(doc, domainid) {
    var sql_1 = " select distinct  '" + domainid + "' as domainid,  (case when (item_xqzxt is null or item_xqzxt = '') then concat(item_xqsxxt, item_dqbb) " +
        " else concat(item_xqsxxt, '-', item_xqzxt, item_dqbb) end) as item_sys_ver from tlk_clause_present where " +
        " parent = '" + doc.getId() + "'";
    var result_1 = queryBySQL(sql_1);
    if (null != result_1) {
        for (var i = 0; i < result_1.size(); i++) {
            var data = result_1.get(i);
            var version = data.getItemValueAsString("sys_ver");
            var sql_2_1 = "select '" + domainid + "' as domainid, item_zhgzl_zj as item_zhgzl_zj, item_wbgzl_zj as item_wbgzl_zj, item_gzlzj as item_gzlzj from tlk_workload_assess_info where item_xtbb = '" + version + "' and parent = '" + doc.getId() + "'"
            var result_2_1 = findBySQL(sql_2_1);
            if (null != result_2_1) {
                var gzl_zj = result_2_1.getItemValueAsString("gzlzj");
                var gzl_zh = result_2_1.getItemValueAsString("zhgzl_zj");
                var gzl_wb = result_2_1.getItemValueAsString("wbgzl_zj");
                var sql_2 = "select '" + domainid + "' as domainid,  ROUND(sum(ct.ITEM_XHTM_XJ), 2) as item_count from tlk_clause_thin ct  " +
                    " inner join tlk_workload_assess_info wa on ct.parent = wa.id and wa.parent = '" + doc.getId() + "'  " +
                    " inner join tlk_clause_present cp on  CONCAT( cp.item_tmbh, '_', cp.item_zt) = ct.item_xhtm_xqtm  " +
                    " and (case when (cp.item_xqzxt is null or cp.item_xqzxt = '') then concat(cp.item_xqsxxt, cp.item_dqbb)   " +
                    " else concat(cp.item_xqsxxt, '-', cp.item_xqzxt, cp.item_dqbb) end) = '" + version + "' ";
                var result_2 = findBySQL(sql_2);
                if (null != result_2) {
                    var sum = result_2.getItemValueAsString("count");
                    var sql_3 = "select  '" + domainid + "' as domainid, id as item_id, CONCAT( item_tmbh, '_', item_zt) as item_title from tlk_clause_present where (case when (item_xqzxt is null or item_xqzxt = '')  " +
                        " then concat(item_xqsxxt, item_dqbb) else concat(item_xqsxxt, '-', item_xqzxt, item_dqbb) end) = '" + version + "'  " +
                        " and parent = '" + doc.getId() + "'";
                    var result_3 = queryBySQL(sql_3);
                    if (null != result_3) {
                        for (var n = 0; n < result_3.size(); n++) {
                            var obj = result_3.get(n);
                            var clauseId = obj.getItemValueAsString("id");
                            var title = obj.getItemValueAsString("title");
                            var sql_4 = "select  '" + domainid + "' as domainid, ROUND(sum(ITEM_XHTM_XJ), 2) as item_clause_sum from tlk_clause_thin ct " +
                                " inner join tlk_clause_present cp on ct.item_xhtm_xqtm = concat(cp.item_tmbh, '_', cp.item_zt) and cp.parent = '" + doc.getId() + "' " +
                                " and cp.id = '" + clauseId + "'";
                            var result_4 = findBySQL(sql_4);
                            if (null != result_4) {
                                var clauseCount = result_4.getItemValueAsString("clause_sum");
                                var rate = clauseCount / sum;
                                var sql_5 = "update tlk_clause_present set item_zh_trl = " + rate * gzl_zh + ", item_trl = " + rate * gzl_zj + ", ITEM_WB_TRL = " + rate * gzl_wb + " where parent = '" + doc.getId() +
                                    "' and concat( item_tmbh, '_', item_zt) = '" + title + "' and " +
                                    " (case when (item_xqzxt is null or item_xqzxt = '') then concat(item_xqsxxt, item_dqbb)  " +
                                    "  else concat(item_xqsxxt, '-', item_xqzxt, item_dqbb) end) = '" + version + "'";
                                updateByDSName(datasource, sql_5);
                            }
                        }
                    }
                }
            }
        }
    }
}


/**
 * 回写签报版本
 */
function writeBackSignVersion(doc) {
    var sql_1 = "update tlk_clause_present cp set cp.ITEM_QBBB = cp.ITEM_DQBB " +
        "   where cp.parent = '" + doc.getId() + "'";
    updateByDSName(datasource, sql_1);
}

/**
 * 计划上线时间
 * 在保存工作量评估的提交后调用
 * @param {工作量评估workload_assess_info} doc 
 */
function writeBackPlanRunTime(doc) {
    var jhsxsj = getItemValueAsString("jhsxsj");
    var sysversion = getItemValueAsString("xtbb");
    var sql = "update tlk_project_ver_plan set item_plan_date = '" + jhsxsj + "' where item_proj_ver = '" + sysversion + "'";
    updateByDSName(datasource, sql);
}


/*
 * 修改条目状态
 */
function updateClauseState(doc, state) {
    var childs = doc.getChilds("budget_back");
    if (null != childs && childs.size() > 0) {
        for (var n = 0; n < childs.size(); n++) {
            var child = childs.get(n);
            var dh = child.getItemValueAsString("tmbh");
            var sql = "";
            if (clause_state.STATE_3.equals(state)) {
                var htl = child.getItemValueAsDouble("htgzl");
                sql = "update tlk_clause_present set item_clause_state = '" + state + "', item_htl = " + htl + " where item_tmbh = ('" + dh + "')";
            } else {
                sql = "update tlk_clause_present set item_clause_state = '" + state + "' where item_tmbh = ('" + dh + "')";
            }
            updateByDSName(datasource, sql);
        }
    }
}

/**
 * 删除预算回退
 */
function deleteYsht() {
    var msg = ""
    var selectId = getParameter("_selects");
    if (isNotNull(selectId)) {
        var domainid = getDomainid();
        var sel = selectId.replace(";", "','");
        var sqll = "SELECT '" + domainid + "'  as domainid,  count(1) as item_count FROM cpmis.tlk_budget_back_list where id in ('" + sel + "')" +
            "and (STATELABEL is not null and STATELABEL != '开始' and STATELABEL != '需求部门发起回退申请')";
        var result_1 = findBySQL(sqll);
        if (null != result_1) {
            var count = result_1.getItemValueAsString("count");
            if (null != count && count != "0") {
                msg = "已经启动的流程不能被删除";
            }
        }
    } else {
        msg = "请选择要删除的电子表单";
    }
    return msg;
}